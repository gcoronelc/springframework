package pe.egcc.app.demo04;

import org.springframework.beans.factory.annotation.Autowired;

public class MateService {

	@Autowired
	private IMate mate;
	
	private String nombre;
	
	@Autowired
	public void setNombre(String nombre) {
	  this.nombre = nombre;
  }
	
	
	public int sumar(int n1, int n2) {
		System.out.println("Hola " + nombre + ".");
		return mate.sumar(n1, n2); 
  }
	
}
