package pe.egcc.app.demo05;

import org.springframework.beans.factory.annotation.Autowired;

public class MateService {

	@Autowired
	private IMate[] lista;

	public void procesar(int n1, int n2) {
		for (IMate bean : lista) {
			System.out.println(n1 + " + " + n2 + " = " + bean.sumar(n1, n2));
		}
	}

}
