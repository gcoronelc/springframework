package pe.egcc.app.demo03;

import org.springframework.beans.factory.annotation.Required;

public class MateService {

	private Mate mate;
	private String nombre;
	
	public void setNombre(String nombre) {
	  this.nombre = nombre;
  }
	
	@Required
	public void setMate(Mate mate) {
	  this.mate = mate;
  }
	
	public int sumar(int n1, int n2) {
		System.out.println("Hola " + nombre + ".");
		return mate.sumar(n1, n2); 
  }
	
}
