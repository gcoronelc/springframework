package pe.egcc.app.demo02;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Prueba {
	
	public static void main(String[] args) {
	  BeanFactory factory;
	  String archivo = "/pe/egcc/app/demo02/contexto.xml";
	  factory = new ClassPathXmlApplicationContext(archivo);
	  MateService service;
	  service = factory.getBean("service",MateService.class);
	  service.procesar();
  }

}
