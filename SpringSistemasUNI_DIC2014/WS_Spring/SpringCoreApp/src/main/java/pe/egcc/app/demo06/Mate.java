package pe.egcc.app.demo06;

import org.springframework.stereotype.Component;

@Component
public class Mate {
	
	public int sumar(int n1, int n2) {
	  int s;
	  s = n1 + n2;
	  return s;
  }

}
