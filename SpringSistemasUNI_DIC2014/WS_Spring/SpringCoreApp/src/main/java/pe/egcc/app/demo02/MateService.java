package pe.egcc.app.demo02;

public class MateService {

	private Mate mate;
	private String nombre;
	private int num1;
	private int num2;
	
	public MateService() {
  }

	public MateService(Mate mate, String nombre, int num1, int num2) {
	  this.mate = mate;
	  this.nombre = nombre;
	  this.num1 = num1;
	  this.num2 = num2;
  }

	public void procesar() {
		System.out.println("Hola " + nombre + ".");
		System.out.println("Número 1: " + num1);
		System.out.println("Número 2: " + num2);
		System.out.println("Suma: " + mate.sumar(num1, num2) ); 
  }
	
}
