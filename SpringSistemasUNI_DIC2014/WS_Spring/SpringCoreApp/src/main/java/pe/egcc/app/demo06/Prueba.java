package pe.egcc.app.demo06;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Prueba {
	
	public static void main(String[] args) {
	  BeanFactory factory;
	  String archivo = "/pe/egcc/app/demo06/contexto.xml";
	  factory = new ClassPathXmlApplicationContext(archivo);
	  MateService service;
	  service = factory.getBean("mateService",MateService.class);
	  System.out.println("Suma: " + service.sumar(14, 16));
  }

}
