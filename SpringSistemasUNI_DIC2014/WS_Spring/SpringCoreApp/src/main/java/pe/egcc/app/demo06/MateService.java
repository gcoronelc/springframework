package pe.egcc.app.demo06;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MateService {

	@Autowired
	private Mate mate;
	
	public int sumar(int n1, int n2) {
		return mate.sumar(n1, n2); 
  }
	
}
