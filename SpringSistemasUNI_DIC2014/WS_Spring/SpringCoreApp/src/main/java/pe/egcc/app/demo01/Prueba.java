package pe.egcc.app.demo01;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Prueba {
	
	public static void main(String[] args) {
	  BeanFactory factory;
	  String archivo = "/pe/egcc/app/demo01/contexto.xml";
	  factory = new ClassPathXmlApplicationContext(archivo);
	  MateService service;
	  service = factory.getBean("service",MateService.class);
	  System.out.println("Suma: " + service.sumar(14, 16));
  }

}
