package pe.egcc.eureka.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.egcc.eureka.dao.CuentaDao;
import pe.egcc.eureka.dao.EurekaDao;
import pe.egcc.eureka.domain.Cliente;
import pe.egcc.eureka.domain.Empleado;

@Service
public class EurekaService {

	@Autowired
	private EurekaDao eurekaDao;
	
	@Autowired
	private CuentaDao cuentaDao;
	
	public int cantCuentas() {
	  return eurekaDao.cantCuentas();
  }

	public List<Empleado> traerEmpleado(String paterno) {	  
	  return eurekaDao.traerEmpleados(paterno);
  }

	public void deposito(String cuenta, double importe, String empleado) {
	  cuentaDao.regDeposito(cuenta, importe, empleado);
  }

	public String consultarEstado(String cuenta) {
	  return cuentaDao.estadoCuenta(cuenta);
  }

	public void retiro(String cuenta, double importe, String clave, String codigo) {
	  cuentaDao.retiro(cuenta, importe, codigo, clave);
  }

	public double consultarSaldo(String cuenta) {
	  return cuentaDao.consultarSaldo(cuenta);
  }

	public List<Cliente> consultarClientes(String nombre) {
	  return cuentaDao.cnsultarClientes(nombre);
  }

	
	
}
