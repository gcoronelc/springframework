package pe.egcc.eureka.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import oracle.jdbc.OracleTypes;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.CallableStatementCallback;
import org.springframework.jdbc.core.CallableStatementCreator;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pe.egcc.eureka.domain.Cliente;

@Repository
public class CuentaDao extends AbstractDao {

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void regDeposito(String cuenta, double importe, String empleado) {
    // Leer datos de la cuenta
    String sql = "select dec_cuensaldo, int_cuencontmov " + "from cuenta "
        + "where chr_cuencodigo = ? for update";
    Map<String, Object> rec = jdbcTemplate.queryForMap(sql,
        new Object[] { cuenta });
    double saldo = Double.parseDouble(rec.get("dec_cuensaldo").toString());
    int cont = Integer.parseInt(rec.get("int_cuencontmov").toString());

    // Actualizar cuenta
    saldo += importe;
    cont++;
    sql = "update cuenta set dec_cuensaldo = ?, "
        + "int_cuencontmov = ? where chr_cuencodigo = ?";
    Object[] parms = { saldo, cont, cuenta };
    jdbcTemplate.update(sql, parms);

    // Registrar movimiento

    sql = "insert into movimiento( chr_cuencodigo,"
        + "int_movinumero, dtt_movifecha, "
        + "chr_emplcodigo, chr_tipocodigo, " + "dec_moviimporte) "
        + "values(?,?,sysdate,?,'003',?)";
    parms = new Object[] { cuenta, cont, empleado, importe };
    jdbcTemplate.update(sql, parms);

  }

  public String estadoCuenta(final String cuenta) {
    String estado = "";

    CallableStatementCreator csc = new CallableStatementCreator() {

      @Override
      public CallableStatement createCallableStatement(Connection cn)
          throws SQLException {
        CallableStatement cs;
        cs = cn.prepareCall("{? = call fn_egcc_estado_cuenta(?)}");
        cs.registerOutParameter(1, Types.VARCHAR);
        cs.setString(2, cuenta);
        return cs;
      }
    };

    CallableStatementCallback<String> action = new CallableStatementCallback<String>() {

      @Override
      public String doInCallableStatement(CallableStatement cs)
          throws SQLException, DataAccessException {
        cs.execute();
        String estado = cs.getString(1);
        return estado;
      }

    };

    estado = jdbcTemplate.execute(csc, action);

    return estado;
  }

  public void retiro(final String cuenta, final double importe,
      final String empleado, final String clave) {

    CallableStatementCreator csc = new CallableStatementCreator() {

      @Override
      public CallableStatement createCallableStatement(Connection cn)
          throws SQLException {
        CallableStatement cs;
        cs = cn.prepareCall("{call usp_egcc_retiro(?,?,?,?)}");
        cs.setString(1, cuenta);
        cs.setDouble(2, importe);
        cs.setString(3, empleado);
        cs.setString(4, clave);
        return cs;
      }
    };

    CallableStatementCallback<Object> action = new CallableStatementCallback<Object>() {

      @Override
      public String doInCallableStatement(CallableStatement cs)
          throws SQLException, DataAccessException {
        cs.execute();
        return null;
      }

    };

    jdbcTemplate.execute(csc, action);
  }

  public double consultarSaldo(String cuenta) {
    SPSaldoCuenta bean = new SPSaldoCuenta(jdbcTemplate);
    return bean.execute(cuenta);
  }

  public List<Cliente> cnsultarClientes(String nombre) {
    SPClientes bean = new SPClientes(jdbcTemplate);
    return bean.execute(nombre);
  }

  public List<Map<String, ?>> conMovimientos(final String cuenta) {

    CallableStatementCreator csc = new CallableStatementCreator() {

      @Override
      public CallableStatement createCallableStatement(Connection cn)
          throws SQLException {
        String sql = "{call usp_egcc_movimientos(?,?)}";
        CallableStatement cs = cn.prepareCall(sql);
        cs.setString(1, cuenta);
        cs.registerOutParameter(2, OracleTypes.CURSOR);
        return cs;
      }
    };

    CallableStatementCallback<List<Map<String, ?>>> action = new CallableStatementCallback<List<Map<String, ?>>>() {

      @Override
      public List<Map<String, ?>> doInCallableStatement(CallableStatement cs)
          throws SQLException, DataAccessException {
        cs.execute();
        ResultSet rs = (ResultSet) cs.getObject(2);
        return JdbcUtil.rsToList(rs);
      }
    };

    return jdbcTemplate.execute(csc, action);
  }

}
