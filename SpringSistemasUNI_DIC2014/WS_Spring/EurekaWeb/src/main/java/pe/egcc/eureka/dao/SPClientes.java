package pe.egcc.eureka.dao;

import java.sql.Types;
import java.util.List;
import java.util.Map;

import oracle.jdbc.internal.OracleTypes;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import pe.egcc.eureka.domain.Cliente;
import pe.egcc.eureka.mapper.ClienteMapper;

public class SPClientes extends StoredProcedure {
	private static final String SPROC_NAME = "usp_egcc_clientes";
	
	public SPClientes(JdbcTemplate jdbcTemplate) {
	  super(jdbcTemplate,SPROC_NAME);
	  declareParameter(new SqlParameter("p_nombre", Types.VARCHAR));
	  declareParameter(new SqlOutParameter(
	  		"p_cursor",OracleTypes.CURSOR, new ClienteMapper()));
	  compile();
  }
	
	public List<Cliente> execute(String nombre) {
	  Map<String, Object> result = super.execute(nombre);
	  List<Cliente> lista = (List<Cliente>) result.get("p_cursor");
	  return lista;
  }
}
