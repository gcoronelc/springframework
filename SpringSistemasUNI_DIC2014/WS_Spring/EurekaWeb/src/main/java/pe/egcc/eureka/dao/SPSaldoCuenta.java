package pe.egcc.eureka.dao;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class SPSaldoCuenta extends StoredProcedure {
	
	private static final String SPROC_NAME = "usp_egcc_saldo_cuenta";
	
	public SPSaldoCuenta(JdbcTemplate jdbcTemplate) {
		super(jdbcTemplate, SPROC_NAME);
		declareParameter(new SqlParameter("p_cuenta", Types.VARCHAR));
		declareParameter(new SqlOutParameter("p_saldo", Types.DECIMAL));
  }

	public double execute(String cuenta) {
		Map<String,?> rpta = super.execute(cuenta);
		BigDecimal saldo = (BigDecimal)rpta.get("p_saldo");
		return saldo.doubleValue();
  }
}
