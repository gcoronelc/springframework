package pe.egcc.eureka.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import pe.egcc.eureka.domain.Empleado;
import pe.egcc.eureka.service.LogonService;

@Controller
//@Scope(value="session")
public class HomeController {
	
	@Autowired
	private LogonService logonService;
	
	private Empleado empleado;
	
	public Empleado getEmpleado() {
		return empleado;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {		
		return "home";
	}
	
	@RequestMapping(value="logon.html", method=RequestMethod.POST)
	public ModelAndView logon(
			@RequestParam("usuario") String usuario,
			@RequestParam("clave") String clave,
			HttpServletRequest request
			){
		ModelAndView model = new ModelAndView();
		try{
			Empleado bean = logonService.validar(usuario,clave);
			empleado = bean;
			request.getSession().setAttribute("usuario", bean);
			model.setViewName("main");
		}catch(Exception e){
			model.setViewName("home");
			model.addObject("mensaje", e.getMessage());
		}
		return model;
	}
}
