package pe.egcc.eureka.dao;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import pe.egcc.eureka.domain.Empleado;
import pe.egcc.eureka.mapper.EmpleadoMapper;

@Repository
public class LogonDao extends AbstractDao {

	public Empleado validar(String usuario, String clave) {
		Empleado bean = null;
	  try {
	    String sql = "select chr_emplcodigo, vch_emplpaterno, "
	    		+ "vch_emplmaterno, vch_emplnombre, vch_emplciudad, "
	    		+ "vch_empldireccion, vch_emplusuario "
	    		+ "from empleado where vch_emplusuario = ? "
	    		+ "and vch_emplclave = ?";
	    Object[] args = {usuario,clave};
	    bean = jdbcTemplate.queryForObject(sql, args, new EmpleadoMapper());
    } catch (EmptyResultDataAccessException e) {
    }
	  return bean;
  }

	
	
}
