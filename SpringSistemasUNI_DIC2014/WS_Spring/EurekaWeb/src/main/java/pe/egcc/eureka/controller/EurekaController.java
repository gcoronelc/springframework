package pe.egcc.eureka.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import pe.egcc.eureka.domain.Cliente;
import pe.egcc.eureka.domain.Empleado;
import pe.egcc.eureka.service.EurekaService;
import pe.egcc.eureka.service.LogonService;

@Controller
public class EurekaController {

	@Autowired
	private EurekaService eurekaService;
	
	@Autowired
	private HomeController homeController;

	@RequestMapping(value = "cantCuentas.html")
	public ModelAndView cantCuentas() {
		ModelAndView model = new ModelAndView("cantCuentas");
		model.addObject("cant", eurekaService.cantCuentas());
		return model;
	}

	@RequestMapping(value = "conEmpleados.html", method = RequestMethod.GET)
	public String conEmpleados() {
		System.out.println("EGCC");
		System.err.println("Empleado: " + homeController.getEmpleado().getCodigo());
		return "consultarEmpleados";
	}

	@RequestMapping(value = "conEmpleados.html", method = RequestMethod.POST)
	public ModelAndView conEmpleados(@RequestParam("paterno") String paterno) {
		ModelAndView model = new ModelAndView("consultarEmpleados");
		model.addObject("paterno", paterno);
		model.addObject("lista", eurekaService.traerEmpleado(paterno));
		return model;
	}

	@RequestMapping(value = "deposito.html", method = RequestMethod.GET)
	public String registraDeposito() {
		return "deposito";
	}

	@RequestMapping(value = "deposito.html", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String registraDeposito(HttpServletRequest request) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		try {
			// Data
			String cuenta = request.getParameter("cuenta");
			double importe = Double
					.parseDouble(request.getParameter("importe"));
			// Usuario
			Empleado bean = (Empleado) request.getSession().getAttribute(
					"usuario");
			// Proceso
			eurekaService.deposito(cuenta, importe, bean.getCodigo());
			// Respuesta
			rpta.put("code", 1);
			rpta.put("mensaje", "Proceso ejecutado correctamente.");
		} catch (Exception e) {
			rpta.put("code", -1);
			rpta.put("mensaje", e.getMessage());
		}
		Gson gson = new Gson();
		return gson.toJson(rpta);
	}
	

	@RequestMapping(value = "estadoDeCuenta.html", method = RequestMethod.GET)
	public String estadoDeCuenta() {
		return "estadoDeCuenta";
	}

	@RequestMapping(value = "estadoDeCuenta.html", method = RequestMethod.POST, produces = "text/plain")
	public @ResponseBody String estadoDeCuenta(HttpServletRequest request) {
		String estado;
		try {
			// Data
			String cuenta = request.getParameter("cuenta");
			// Proceso
			estado = eurekaService.consultarEstado(cuenta);
		} catch (Exception e) {
			estado = e.getMessage();
		}
		return estado;
	}

	@RequestMapping(value = "retiro.html", method = RequestMethod.GET)
	public String retiro() {
		return "retiro";
	}

	@RequestMapping(value = "retiro.html", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody String retiro(HttpServletRequest request) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		try {
			// Data
			String cuenta = request.getParameter("cuenta");
			double importe = Double
					.parseDouble(request.getParameter("importe"));
			String clave = request.getParameter("clave");
			// Usuario
			Empleado bean = (Empleado) request.getSession().getAttribute(
					"usuario");
			// Proceso
			eurekaService.retiro(cuenta, importe, clave, bean.getCodigo());
			// Respuesta
			rpta.put("code", 1);
			rpta.put("mensaje", "Proceso ejecutado correctamente.");
		} catch (Exception e) {
			rpta.put("code", -1);
			rpta.put("mensaje", e.getMessage());
		}
		Gson gson = new Gson();
		return gson.toJson(rpta);
	}

	@RequestMapping(value = "saldoDeCuenta.html", method = RequestMethod.GET)
	public String saldoDeCuenta() {
		return "saldoDeCuenta";
	}

	@RequestMapping(value = "saldoDeCuenta.html", method = RequestMethod.POST, produces = "text/plain")
	public @ResponseBody String saldoDeCuenta(HttpServletRequest request) {
		String estado;
		try {
			// Data
			String cuenta = request.getParameter("cuenta");
			// Proceso
			double saldo = eurekaService.consultarSaldo(cuenta);
			estado = "SALDO: " + saldo;
		} catch (Exception e) {
			estado = "ERROR: " + e.getMessage();
		}
		return estado;
	}

	@RequestMapping(value = "mantClientes.html", method = RequestMethod.GET)
	public String mantClientes() {
		return "mantClientes";
	}

	@RequestMapping(value = "mantClientes.html", method = RequestMethod.POST)
	public ModelAndView mantClientes(HttpServletRequest request) {
		ModelAndView model = new ModelAndView("mantClientes");
		try {
			// Data
			String nombre = request.getParameter("nombre");
			// Proceso
			List<Cliente> lista = eurekaService.consultarClientes(nombre);
			if (!lista.isEmpty()) {
				model.addObject("lista", lista);
			}
			model.addObject("nombre", nombre);
		} catch (Exception e) {
			model.addObject("error", e.getMessage());
		}
		return model;
	}
}
