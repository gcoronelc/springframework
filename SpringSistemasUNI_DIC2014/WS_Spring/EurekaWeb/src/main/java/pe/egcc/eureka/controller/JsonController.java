package pe.egcc.eureka.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import pe.egcc.eureka.domain.Cliente;
import pe.egcc.eureka.domain.Empleado;
import pe.egcc.eureka.service.EurekaService;

@Controller
public class JsonController {
	
	@Autowired
	private EurekaService eurekaService;
	

	@RequestMapping(value = "json1.html", method = RequestMethod.GET)
	public @ResponseBody Map<String, Object> json1() {
		
		Map<String, Object> rpta = new HashMap<String, Object>();
		rpta.put("dato1", 4567);
		rpta.put("dato2", "Hola");
		rpta.put("dato3", new Date());
		rpta.put("dato4", "Hola todos");
		
		// Gson gson = new Gson();
		// return gson.toJson(rpta);
		
		return rpta;
	}
	
	@RequestMapping(value = "json2.html", method = RequestMethod.GET)
	public @ResponseBody String json2() {
		
		
		JsonArray jsonItems = new JsonArray();

	    JsonObject jsonItem1 = new JsonObject();
	    jsonItem1.addProperty("id", "1");
	    jsonItem1.addProperty("name", "My T�st Project");

	    JsonObject jsonItem2 = new JsonObject();
	    jsonItem2.addProperty("id", "4");
	    jsonItem2.addProperty("name", "Another one");

	    jsonItems.add(jsonItem1);
	    jsonItems.add(jsonItem2);

	    return jsonItems.toString();
	}
	
	@RequestMapping(value = "json3.html", method = RequestMethod.GET)
	public @ResponseBody List<Empleado> json3() {
		
		List<Empleado> lista;
		
		lista = eurekaService.traerEmpleado("%");

	    return lista;
	}

}
