package pe.egcc.eureka.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import pe.egcc.eureka.domain.Empleado;
import pe.egcc.eureka.mapper.EmpleadoMapper;

@Repository
public class EurekaDao extends AbstractDao {

	public int cantCuentas() {
		String sql = "select count(*) from cuenta";
		int rowCount = jdbcTemplate.queryForObject(sql, Integer.class);
	  return rowCount;
  }

	public List<Empleado> traerEmpleados(String paterno) {
		String sql = "select chr_emplcodigo, vch_emplpaterno, "
		    + "vch_emplmaterno, vch_emplnombre, vch_emplciudad, "
		    + "vch_empldireccion, vch_emplusuario "
		    + "from empleado where vch_emplpaterno like ?";
		Object[] parms = { paterno + "%" };
		List<Empleado> lista = jdbcTemplate.query(sql, parms, new EmpleadoMapper());
	  return lista;
  }

}
