package pe.egcc.eureka.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.egcc.eureka.dao.LogonDao;
import pe.egcc.eureka.domain.Empleado;

@Service
public class LogonService {

	@Autowired
	private LogonDao logonDao;
	
	public Empleado validar(String usuario, String clave) {
	  if(usuario.isEmpty()){
	  	throw new RuntimeException("Faltan datos.");
	  }
	  if(clave.isEmpty()){
	  	throw new RuntimeException("Faltan datos.");
	  }
	  Empleado bean = logonDao.validar(usuario,clave);
	  if(bean == null){
	  	throw new RuntimeException("Datos incorrectos.");
	  }
	  return bean;
  }

}
