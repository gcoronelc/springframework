<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link type="text/css" rel="stylesheet" href='<c:url value="/resources/css/estilos.css"/>' >
<link type="text/css" rel="stylesheet" href='<c:url value="/resources/menu/menu.css"/>' >
<title>EUREKA APP</title>
</head>
<body>
  <div class="_PAGE">
    <div class="_HEADER">
    	<table width="100%">
    		<tr>
    			<td><img alt="" src="<c:url value='/resources/img/logo.gif' />" /></td>
    			<td>
    				<jsp:include page="menu.jsp"/>    			
    			</td>
    			<td>
    				${sessionScope.usuario.nombre} ${sessionScope.usuario.paterno}<br/>
    				<a href="#">Salir</a> 
    			</td>
    		</tr>
    	</table>
    </div>
	<div id="_BODY" class="_BODY">
	<br/>
	<br/>
	<br/>
    </div>
    <div class="_FOOTER">
    	Derechos reservados @ 2015 - EGCC
    </div>
  </div>
</body>
<script type="text/javascript" 
	src="<c:url value='/resources/jquery/jquery-1.11.2.min.js'/>"></script>
<script type="text/javascript">

  function fnCargarPagina(pagina){
	$("#_BODY").load(pagina);
  }	

</script>	

</html>