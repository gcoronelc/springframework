<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>REGISTRA DEPOSITO</title>
</head>
<body>
  <form id="form1">
    
    <h1>REGISTRA DEPOSITO</h1>
    
    <label>Cuenta:</label>
    <input type="text" name="cuenta" placeholder="N�mero de cuenta.">
    <hr/>
    
    <label>Importe:</label>
    <input type="text" name="importe" placeholder="Importe a depositar.">
    <hr/>
    
    <input type="button" id="btnProcesar" value="Procesar">
  </form>
</body>

<script type="text/javascript">

  $("#btnProcesar").click(function(){
	  var data = $("#form1").serialize();
	  $.post("deposito.html",data,function(objJson){
		  alert(objJson.mensaje);
	  });
  });

</script>

</html>