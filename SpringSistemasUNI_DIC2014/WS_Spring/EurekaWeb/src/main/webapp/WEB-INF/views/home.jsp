<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title>Home</title>
	<link type="text/css" rel="stylesheet" 
		href='<c:url value="/resources/css/estilos.css"/>' >
	<style type="text/css">
		#_LOGON{
			width: 250px;
			background-color: white;
			margin: 10px auto;
			padding: 10px;
		}
	</style>
</head>
<body>
	<div id="_LOGON">
	<h1>INGRESO AL SISTEMA</h1>
	<c:if test="${mensaje != null}">
		<p class="_error">${mensaje}</p>
	</c:if>
	<form method="post" action="logon.html">
		
		Usuario:<br/> 
		<input type="text" name="usuario"><br/>
		
		Clave: <br/>
		<input type="password" name="clave"><br/>
		
		<br/>
		<input type="submit" value="Ingresar" />
	</form>
	</div>
</body>
</html>
