<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MANTENIMIENTO DE CLIENTES</title>
</head>
<body>
  <h1>MANTENIMIENTO DE CLIENTES</h1>
  <form id="form1">
    <label>Nombre:</label><br/>
    <input type="text" name="nombre" value="${nombre}" placeholder="Nombre del cliente."/>
    <input type="button" id="btnBuscar" value="Buscar">
  </form>
  <hr/>
  <div id="divResultado">
    <c:if test="${lista != null}">
      <table border="1">
        <tr>
          <th>CODIGO</th>
          <th>PATERNO</th>
          <th>MATERNO</th>
          <th>NOMBRE</th>
        </tr>
        
        <c:forEach items="${lista}" var="r">
	       <tr>
	        <td>${r.codigo}</td>
	        <td>${r.paterno}</td>
	        <td>${r.materno}</td>
	        <td>${r.nombre}</td>
	       </tr>
        </c:forEach>
        
      </table>
    </c:if>
  </div>
  
</body>

<script type="text/javascript">
  $("#btnBuscar").click(function(){
    var data = $("#form1").serialize();
    $.post("mantClientes.html",data,function(html){
      $("#_BODY").html(html); 
    });
  });
</script>

</html>