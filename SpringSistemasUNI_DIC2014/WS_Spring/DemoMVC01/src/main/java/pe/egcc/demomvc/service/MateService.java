package pe.egcc.demomvc.service;

import org.springframework.stereotype.Service;

@Service
public class MateService {

	public int calcPromedio(int n1, int n2, int n3) {
		int pr;
		pr = (n1 + n2 + n3) / 3;
		return pr;
	}

	
	public int elMayor(int n1, int n2, int n3) {
	  int ma;
	  ma = Math.max(n1, n2);
	  ma = Math.max(ma, n3);
	  return ma;
  }
}
