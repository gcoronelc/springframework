package pe.egcc.demomvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/receta")
public class RecetaController {

	@RequestMapping(method = RequestMethod.GET) 
	public ModelAndView handler() {
		ModelAndView model = new ModelAndView("receta");
		model.addObject("mensaje", "Listado");
		return model;
	}	
	
	@RequestMapping(value="/nueva", method = RequestMethod.GET) 
	public ModelAndView nuevaHandler() {
		ModelAndView model = new ModelAndView("receta");
		model.addObject("mensaje", "Nueva receta.");
		return model;
	}
}
