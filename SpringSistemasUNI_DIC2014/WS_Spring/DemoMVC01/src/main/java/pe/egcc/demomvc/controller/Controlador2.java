package pe.egcc.demomvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Controlador2 {

	@RequestMapping(value="ejerc2.htm", params={"createForm"})
	public String ejer2Form() {
	  return "ejerc2form";
  }
	
	@RequestMapping(value="ejerc2.htm", params={"!createForm"})
	public ModelAndView ejer2Form(
			@RequestParam("num1") int n1,
			@RequestParam("num2") int n2
			) {
		int mcd = calcMcd(n1,n2);
		int mcm = calcMcm(n1,n2);
		ModelAndView model = new ModelAndView("ejerc2rpta");
		model.addObject("n1", n1);
		model.addObject("n2", n2);
		model.addObject("mcd", mcd);
		model.addObject("mcm", mcm);
	  return model;
  }

	private int calcMcd(int n1, int n2) {
		while(n1 != n2){
			if(n1 > n2){
				n1 -= n2;
			} else {
				n2 -= n1;
			}
		}
	  return n1;
  }

	private int calcMcm(int n1, int n2) {
	  return n1 * n2 / ( calcMcd(n1, n2));
  }
	
}
