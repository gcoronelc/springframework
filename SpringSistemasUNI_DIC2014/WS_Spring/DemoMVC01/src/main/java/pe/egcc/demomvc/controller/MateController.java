package pe.egcc.demomvc.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import pe.egcc.demomvc.service.MateService;

@Controller
public class MateController {

	@Autowired
	private MateService mateService;
	
	@RequestMapping(value="/calculaPromedio.htm") 
	public ModelAndView calculaPromedio( 
			@RequestParam("n1") int n1, 
			@RequestParam("n2") int n2, 
			@RequestParam("n3") int n3 ){ 
		// Proceso 
		int pr; 
		pr = mateService.calcPromedio(n1, n2, n3); 
		// Preparando el modelo 
		ModelAndView model = new ModelAndView("promedioRpta"); 
		model.addObject("nota1", n1); 
		model.addObject("nota2", n2); 
		model.addObject("nota3", n3); 
		model.addObject("promedio", pr); 
		// Retornando el modelo 
		return model; 
	}
	
	@RequestMapping(value="elMayor.htm", params={"!n1"})
	public String elMayor() {
	  return "elMayor";
  }
	
	@RequestMapping(value="elMayor.htm", params={"n1"})
	public @ResponseBody String elMayor(
			@RequestParam("n1") int n1,
			@RequestParam("n2") int n2,
			@RequestParam("n3") int n3){
		int ma = mateService.elMayor(n1, n2, n3);
		Map<String,Object> datos = new HashMap<String, Object>();
		datos.put("n1", n1);
		datos.put("n2", n2);
		datos.put("n3", n3);
		datos.put("ma", ma);
		Gson gson = new Gson();
		String jsonString = gson.toJson(datos);
		System.out.println("EGCC: " + jsonString);
		return jsonString;
	}
}
