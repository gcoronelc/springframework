package pe.egcc.demomvc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import pe.egcc.demomvc.domain.Cliente;

@Controller
public class HomeController {
	

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {
		model.addAttribute("nombre", "Gustavo Coronel" );
		model.addAttribute("mensaje", "Feliz a�o 2015." );
		return "home";
	}
	
	@RequestMapping(value="/cuadro.htm")
	public ModelAndView alCuadrado(HttpServletRequest request) {
	  int n = Integer.parseInt(request.getParameter("n"));
	  int c = n * n;
	  ModelAndView model = new ModelAndView("cuadrado");
	  model.addObject("num", n);
	  model.addObject("cua", c);
	  return model;
  }
	
	@RequestMapping(value="/cliente/{codigo:[0-9]+}")
	public ModelAndView traerCliente(@PathVariable("codigo") int codigo) {
	  ModelAndView model = new ModelAndView("cliente");
	  model.addObject("codigo", codigo);
	  return model;
  }
	
	
	@RequestMapping(value="/sumaForm.htm")
	public String sumaForm(){
		return "sumaForm";
	}
	
	@RequestMapping(value="/sumar.htm")
	public ModelAndView sumar(
			@RequestParam("num1") int n1,
			@RequestParam(value="num2", required=false) Integer n2) {
		if(n2 == null){
			n2 = 0;
		}
		int suma = n1 + n2;
		ModelAndView model = new ModelAndView("sumar");
	  model.addObject("num1", n1);
	  model.addObject("num2", n2);
	  model.addObject("suma", suma);
	  return model;
  }
	
	@RequestMapping(value="clienteForm.htm")
	public ModelAndView showClienteForm() {
		Cliente bean = new Cliente();
		bean.setNombre("GUSTAVO");
		bean.setApellidos("CORONEL CASTILLO");
	  ModelAndView model = new ModelAndView("clienteForm","cliente",bean);
	  return model;
  }
	
	@RequestMapping(value="clienteProcess.htm")
	public ModelAndView processClienteForm(Cliente bean) {
	  ModelAndView model = new ModelAndView("clienteRpta","bean",bean);
	  return model;
  }
	

	
}
