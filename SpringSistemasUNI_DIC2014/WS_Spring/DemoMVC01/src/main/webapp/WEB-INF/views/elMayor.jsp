<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EL MAYOR</title>
</head>
<body>
	<h1>EL MAYOR</h1>
	<form name="form1" id="form1">
		N�mero 1: <input type="text" name="n1" /><br/>
		N�mero 2: <input type="text" name="n2" /><br/>
		N�mero 3: <input type="text" name="n3" /><br/>
		<input type="button" value="Procesar" id="btnProcesar"/><br/>
		Mayor: <span id="mayor"></span>
	</form>
</body>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
	$("#btnProcesar").click(function(){
		var data = $("#form1").serialize();
		$.post("elMayor.htm",data,function(dataJson){
			//alert(dataJson);
			var obj = eval("(" + dataJson + ")")
			$("#mayor").html(obj.ma);
		});
	});
</script>
</html>