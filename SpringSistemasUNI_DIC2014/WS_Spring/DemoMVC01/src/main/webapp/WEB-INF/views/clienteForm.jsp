<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CLIENTE FORM</title>
</head>
<body>
	<h1>CLIENTE FORM</h1>
	<form:form commandName="cliente" method="post" action="clienteProcess.htm">
		<form:label path="nombre">Nombre:</form:label>
		<form:input path="nombre"/><br/>
		<form:label path="apellidos">Apellidos:</form:label>
		<form:input path="apellidos"/> <br/>
		<input type="submit"/>
	</form:form>
</body>
</html>