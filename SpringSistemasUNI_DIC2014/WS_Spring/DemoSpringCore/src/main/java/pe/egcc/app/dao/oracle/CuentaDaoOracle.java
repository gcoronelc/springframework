package pe.egcc.app.dao.oracle;

import org.springframework.stereotype.Repository;

import pe.egcc.app.dao.interfaz.CuentaDao;

@Repository
public class CuentaDaoOracle implements CuentaDao {

	@Override
	public void crearCuenta() {
		System.out.println("Cuenta creada en oracle.");
	}

}
