package pe.egcc.app.dao.mysql;

import org.springframework.stereotype.Repository;

import pe.egcc.app.dao.interfaz.CuentaDao;

@Repository
public class CuandoDaoMySQL implements CuentaDao{

	@Override
  public void crearCuenta() {
	  System.out.println("Cuenta creada en mysql.");
  }

}
