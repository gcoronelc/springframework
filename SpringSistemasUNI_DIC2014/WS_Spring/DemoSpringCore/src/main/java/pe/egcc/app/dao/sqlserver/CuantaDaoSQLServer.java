package pe.egcc.app.dao.sqlserver;

import org.springframework.stereotype.Repository;

import pe.egcc.app.dao.interfaz.CuentaDao;

@Repository
public class CuantaDaoSQLServer implements CuentaDao {

	@Override
	public void crearCuenta() {
		System.out.println("Cuenta creada en SQL Server.");
	}

}
