package pe.egcc.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.egcc.app.dao.interfaz.CuentaDao;

@Service
public class CuentaService {

	@Autowired
	private CuentaDao cuentaDao;
	
	public void crearCuenta() {
	  cuentaDao.crearCuenta();
  }
}
