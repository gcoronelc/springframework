package pe.egcc.app.prueba;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pe.egcc.app.service.CuentaService;

public class Prueba01 {

	public static void main(String[] args) {
		BeanFactory factory;
		factory = new ClassPathXmlApplicationContext("contexto.xml");
		CuentaService service = factory.getBean(CuentaService.class);
		service.crearCuenta();
	}

}
