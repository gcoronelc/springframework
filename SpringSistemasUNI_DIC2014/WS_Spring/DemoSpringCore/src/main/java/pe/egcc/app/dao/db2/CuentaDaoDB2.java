package pe.egcc.app.dao.db2;

import org.springframework.stereotype.Repository;

import pe.egcc.app.dao.interfaz.CuentaDao;

@Repository
public class CuentaDaoDB2 implements CuentaDao {

	@Override
	public void crearCuenta() {
		System.out.println("Cuenta creada en DB2.");
	}

}
