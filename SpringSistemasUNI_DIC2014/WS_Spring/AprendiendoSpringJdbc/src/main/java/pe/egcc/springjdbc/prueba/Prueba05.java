package pe.egcc.springjdbc.prueba;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import pe.egcc.springjdbc.domain.Empleado;
import pe.egcc.springjdbc.mapper.EmpleadoMapper;

public class Prueba05 {

	public static void main(String[] args) {
		// Definir la fuente de datos
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		dataSource.setUrl("jdbc:oracle:thin:@localhost:1521:XE");
		dataSource.setUsername("eureka");
		dataSource.setPassword("admin");
		// Crear el JdbcTemplate
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		// Prueba: listar los registros de una tabla con RowMapper
		String sql = "select chr_emplcodigo, vch_emplpaterno, "
		    + "vch_emplmaterno, vch_emplnombre, vch_emplciudad, "
		    + "vch_empldireccion, vch_emplusuario "
		    + "from empleado where vch_emplpaterno like ?";
		Object[] parms = { "Rxxx%" };
		List<Empleado> lista = jdbcTemplate.query(sql, parms, new EmpleadoMapper());
		for (Empleado bean : lista) {
			System.out.println(bean.getCodigo() + " " + bean.getNombre() + " "
			    + bean.getPaterno() + " " + bean.getMaterno());
		}
	}

}
