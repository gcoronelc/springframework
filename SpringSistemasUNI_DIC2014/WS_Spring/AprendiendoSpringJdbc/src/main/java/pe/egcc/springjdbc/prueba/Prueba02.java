package pe.egcc.springjdbc.prueba;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class Prueba02 {

	public static void main(String[] args) {
		// Definir la fuente de datos
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		dataSource.setUrl("jdbc:oracle:thin:@localhost:1521:XE");
		dataSource.setUsername("eureka");
		dataSource.setPassword("admin");
		// Crear el JdbcTemplate
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		// Prueba: leer un dato double
		String sql = "select sum(dec_cuensaldo) from cuenta";
		Double saldo = jdbcTemplate.queryForObject(sql, Double.class);
		System.out.println("Saldo total: " + saldo);
	}

}
