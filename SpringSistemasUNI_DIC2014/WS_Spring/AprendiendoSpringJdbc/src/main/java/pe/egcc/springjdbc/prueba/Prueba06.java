package pe.egcc.springjdbc.prueba;

import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class Prueba06 {

	public static void main(String[] args) {
		// Definir la fuente de datos
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		dataSource.setUrl("jdbc:oracle:thin:@localhost:1521:XE");
		dataSource.setUsername("eureka");
		dataSource.setPassword("admin");
		// Crear el JdbcTemplate
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		// Prueba: Uso de Map
		String sql = "select m.chr_cuencodigo cuenta, "
				+ "sum(case when t.vch_tipoaccion = 'INGRESO' "
				+ "then dec_moviimporte else 0.0 end) ingreso, "
				+ "sum(case when t.vch_tipoaccion = 'SALIDA' "
				+ "then dec_moviimporte else 0.0 end) salida "
				+ "from tipomovimiento t "
				+ "join movimiento m "
				+ "on t.chr_tipocodigo = m.chr_tipocodigo "
				+ "where m.chr_cuencodigo = ? "
				+ "group by m.chr_cuencodigo";
		Object[] parms = { "00100001" };
		Map<String,Object> rec;
		rec = jdbcTemplate.queryForMap(sql, parms);
		for (String key: rec.keySet() ) {
			System.out.println(key + "\t" + rec.get(key));
		}
	}

}
