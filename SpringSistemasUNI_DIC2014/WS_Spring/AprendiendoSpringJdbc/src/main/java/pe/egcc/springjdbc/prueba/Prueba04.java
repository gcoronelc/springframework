package pe.egcc.springjdbc.prueba;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import pe.egcc.springjdbc.domain.Cliente;
import pe.egcc.springjdbc.mapper.ClienteMapper;

public class Prueba04 {

	public static void main(String[] args) {
		try {
			// Definir la fuente de datos
			DriverManagerDataSource dataSource = new DriverManagerDataSource();
			dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
			dataSource.setUrl("jdbc:oracle:thin:@localhost:1521:XE");
			dataSource.setUsername("eureka");
			dataSource.setPassword("admin");
			// Crear el JdbcTemplate
			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			// Prueba: leer un registro de una tabla con RowMapper
			String sql = "select chr_cliecodigo, vch_cliepaterno, vch_cliematerno, "
			    + "vch_clienombre, chr_cliedni, vch_clieciudad, vch_cliedireccion, "
			    + "vch_clietelefono, vch_clieemail "
			    + "from cliente where chr_cliecodigo = ?";
			Object[] parms = { "00001" };
			Cliente bean = jdbcTemplate.queryForObject(sql, parms, new ClienteMapper());
			System.out.println("Nombre: " + bean.getNombre());
			System.out.println("Paterno: " + bean.getPaterno());
			System.out.println("Materno: " + bean.getMaterno());
		} catch (EmptyResultDataAccessException e){
			System.err.println("Código no existe.");
		}
	}

}
