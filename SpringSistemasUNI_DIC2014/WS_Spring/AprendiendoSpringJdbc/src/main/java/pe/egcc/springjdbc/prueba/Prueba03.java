package pe.egcc.springjdbc.prueba;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class Prueba03 {

	public static void main(String[] args) {
		// Definir la fuente de datos
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		dataSource.setUrl("jdbc:oracle:thin:@localhost:1521:XE");
		dataSource.setUsername("eureka");
		dataSource.setPassword("admin");
		// Crear el JdbcTemplate
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		// Prueba: leer un dato String
		String sql = "select vch_clienombre from cliente "
				+ "where chr_cliecodigo = ?";
		Object[] parms = { "00001" };
		String nombre = jdbcTemplate.queryForObject(sql, parms, String.class);
		System.out.println("Nombre: " + nombre);
	}

}
