package pe.egcc.app;

public class Saludo {

	private String mensaje;
	
	public Saludo() {
	  mensaje = "Hola causa.";
	  System.err.println("EGCC: Constructor por defecto.");
  }
	
	public Saludo(String mensaje) {
	  this.mensaje = mensaje;
	  System.err.println("EGCC: Constructor con parametro.");
  }

	public void setMensaje(String mensaje) {
	  this.mensaje = mensaje;
	  System.err.println("EGCC: Método set.");
  }

	public void saludar() {
	  System.out.println(mensaje);
  }
	
}
