package pe.egcc.app;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Prueba01 {

	public static void main(String[] args) {
		BeanFactory beanFactory;
		beanFactory = new ClassPathXmlApplicationContext("saludo.xml");
		Saludo bean = (Saludo) beanFactory.getBean("panchito");
		bean.saludar();
	}

}
