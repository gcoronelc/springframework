package pe.egcc.app;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Prueba02 {

	public static void main(String[] args) {
		BeanFactory beanFactory;
		beanFactory = new ClassPathXmlApplicationContext("saludo.xml");
		Saludo bean = beanFactory.getBean("panchito2",Saludo.class);
		bean.saludar();
	}

}
