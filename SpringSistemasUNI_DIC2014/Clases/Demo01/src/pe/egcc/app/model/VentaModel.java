package pe.egcc.app.model;

/**
 *
 * @author Gustavo Coronel
 */
public class VentaModel {

  private Descuento descuento;

  public void setDescuento(Descuento descuento) {
    this.descuento = descuento;
  }

  public double calcularImporte(double precio, int cant) {
    double importe, dcto;
    dcto = precio * descuento.obtenerDescuento(cant) / 100;
    importe = (precio - dcto) * cant;
    return importe;
  }

}
