package pe.egcc.app.model;

/**
 *
 * @author Gustavo Coronel
 */
public class Descuento {

  public double obtenerDescuento(int cant){
    double descuento;
    if(cant < 12){
      descuento = 0.0;
    } else if(cant <= 12){
      descuento = 5.0;
    } else if( cant <= 24){
      descuento = 10.0;
    } else {
      descuento = 15.0;
    }
    return descuento;
  }
  
}
