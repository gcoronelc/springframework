package pe.egcc.app.prueba;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pe.egcc.app.model.VentaModel;

/**
 *
 * @author Gustavo Coronel
 */
public class Prueba01 {

  public static void main(String[] args) {
    // Instanciar el contexto
    String archivo = "/contexto.xml";
    BeanFactory beanFactory;
    beanFactory = new ClassPathXmlApplicationContext(archivo);
    // Obtener el bean
    VentaModel model;
    model = beanFactory.getBean(VentaModel.class);
    System.out.println("Venta: " + model.calcularImporte(345.60, 20));
  }
}
